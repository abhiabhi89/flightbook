import { AppComponent } from './app.component';

import { HttpModule } from '@angular/http';

import { async,inject, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FormsModule, ReactiveFormsModule,FormGroup } from '@angular/forms';
import { FlightComponent } from './flight/flight.component';
import { FlightService } from "./flight/flight.service";
import { DatePipe } from '@angular/common';



describe('FlightComponent', function () {
		console.log('d11');
		let de: DebugElement;
	    let comp: FlightComponent;
	  
	    let fixture: ComponentFixture<FlightComponent>;
	  

	  beforeEach(async(() => {
		TestBed.configureTestingModule({
		  imports: [HttpModule,ReactiveFormsModule, FormsModule],	
		  declarations: [ FlightComponent ]
		})
		.overrideComponent(FlightComponent, {
			set: {
			providers: [FlightService,DatePipe]
		  }
		})
		.compileComponents()
		  .then(() => {
			fixture = TestBed.createComponent(FlightComponent);
			comp = fixture.componentInstance;
		  });
	
	  }));
	  
	 
	  
	  it('booking form invalid when empty', () => {
		  expect(comp.complexForm.invalid).toBeFalsy();
		});
	  
	  
	
		
   	it('submitting a form emits a user', () => {
		comp.complexForm.controls['from'].setValue("BOM");
		comp.complexForm.controls['to'].setValue("DEL");
		comp.complexForm.controls['start'].setValue("02/27/2018");
		comp.complexForm.controls['n_adult'].setValue("1");
		expect(comp.complexForm.valid).toBeTruthy();

		let resp = comp.actionSearch(comp.complexForm.value);
		
		
		
	  });

});