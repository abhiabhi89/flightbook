import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';

import { AlertModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NouisliderModule } from 'ng2-nouislider';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlightComponent } from './flight/flight.component';
import { FlightService } from "./flight/flight.service";
import { DatePipe } from '@angular/common';

@NgModule({
  imports:      [ AlertModule.forRoot(),FormsModule, ReactiveFormsModule, BrowserModule,HttpModule,TabsModule.forRoot(),BsDatepickerModule.forRoot(),NouisliderModule],
  declarations: [ AppComponent,FlightComponent],
  bootstrap:    [ AppComponent ],
  providers: [FlightService,DatePipe]
})
export class AppModule { }
