export class Flight {
   start: string;
   end: string;
   from: string;
   to: string;
   min_price: string;
   max_price: string;
   n_adult: number;
   n_child: number;
   constructor() { 
   }
} 