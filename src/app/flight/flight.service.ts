import {Injectable} from '@angular/core';
import {Http, Response,Headers, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import { Flight } from './flight';


@Injectable()
export class FlightService {
	
	constructor(private http:Http) { }
	
	public getFlightsJson(params:any) {	  
		return this.http.get('app/json/dummy.json')
		   .toPromise()
			.then(this.extractData)
				.catch(this.handleErrorPromise);
	}
	getFlights22(params:any): Observable<Flight[]> {	  
		return this.http.get('app/json/flights.json')
		   .map(this.extractData)
		   .catch(this.handleErrorObservable);
    }
	  
	public getFlights(params:any){
				
		const API_KEY = 'AIzaSyCgILgw9s6w_9zPNTTyXN1w6x_9zKsrFEU';
		const URL = 'https://www.googleapis.com/qpxExpress/v1/trips/search';

		let body = '{"request": {"passengers": {"adultCount": '+params.n_adult+'},"slice": [{"origin": "'+params.from+'","destination": "'+params.to+'","date": "'+params.start+'"},{"origin": "'+params.from+'","destination": "'+params.to+'","date": "'+params.end+'"}]}}';
				
		//let body = '{"request": { "slice": [{"origin": "PVD","destination": "MCO","date": "2018-08-04"},{"destination": "PVD",}],"passengers": {"adultCount": 1,"infantInLapCount": 0,"infantInSeatCount": 0,"childCount": 0,"seniorCount": 0},"solutions": 20,"refundable": "false"}}';
		let headers = new Headers();
		headers.set('Content-Type', 'application/json; charset=utf-8');

		let search = new URLSearchParams();
		search.set('key', API_KEY);

		return this.http
			.post(URL, '', {
				headers: headers,
				search: search,
				body: body,
			}).toPromise()
			.then(this.extractData)
				.catch(this.handleErrorPromise);
    }
  
    private extractData(res: Response) {
		let body = res.json();
		
		console.log(body,'body333')
        return body || {};
    }
	
    private handleErrorObservable (error: Response | any) {	
		return Observable.throw(error.message || error);
    }
	
    private handleErrorPromise (error: Response | any) {	
		return Promise.reject(error.message || error);
    }	
}