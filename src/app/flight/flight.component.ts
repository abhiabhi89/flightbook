import { Component ,OnInit,ViewChild } from '@angular/core';
import { FlightService } from "./flight.service";
import { Flight } from './flight';
import { FormBuilder, FormControl, FormGroup,Validators  } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { TabsetComponent } from 'ngx-bootstrap';


@Component({
	selector: 'flight-app',
	templateUrl: './flight.component.html',
	styleUrls: ['./flight.component.css']
})

export class FlightComponent implements OnInit{
 
	@ViewChild('staticTabs') staticTabs: TabsetComponent;
		 
	flights: Flight[];
	errorMessage: String;
	activeTab: String;
	rowMesz: String;
	locationOption: any;
	returnDate: Boolean;
	adultCount:any = [1,2,3,4,5];
	n_adult = this.adultCount[0];
	minDate = new Date();
	minDate1 = new Date();
	someRange = [0, 20000];
	sliderMin = this.someRange[0];
	sliderMax = this.someRange[1];
	params:any=[];
	flightss:any=[];
	realservice:Boolean=true;
	 
	 
	constructor(private flightService: FlightService,fb: FormBuilder,public datepipe: DatePipe) {
	 
		this.flightService = flightService;		 
		this.activeTab="1";
		this.returnDate = false;		 
		this.rowMesz = 'Please enter your search criteria and click submit.';		 
		this.locationOption = [{id:"BOM",value:"Mumbai - BOM"},{id:"KOL",value:"Kolkatta - KOL"},{id:"DEL",value:"Delhi - DEL"},{id:"BLR",value:"Bangalore - BLR"},{id:"PUN",value:"Pune - PUN"}];
		 
	}
		 
	changeMinDate(date:any){
		this.minDate1 = date;
	}

	ngOnInit() {  
	 
	}
	
	changeService(val:any){
		this.realservice= !this.realservice;
	}
	 
	saveRange(slider:any, value:any) {	   
	   this.sliderMin = value[0];
	   this.sliderMax = value[1];
	}
	 
		
	  
	actionSearch(bookingForm:any): void {
		 
		this.rowMesz = "Please wait...";
		
		this.params.from = bookingForm.from;
		this.params.to = bookingForm.to;
		this.params.n_adult = bookingForm.n_adult;
		this.params.start = this.datepipe.transform(bookingForm.start, 'yyyy-MM-dd');
		 
		if(typeof bookingForm.end == 'undefined'){
			bookingForm.end = bookingForm.start;
		}
		this.params.end = this.datepipe.transform(bookingForm.end, 'yyyy-MM-dd');
		if(typeof bookingForm.someRange !== 'undefined'){
			this.params.min_price = bookingForm.someRange[0];
			this.params.max_price = bookingForm.someRange[1];
		}
		this.params.activeTab = this.activeTab;
		 
		 
		if(this.realservice){
			this.flightService.getFlights(this.params).then( flights => this.successResp(flights),error => this.errorMessage = <any>error);   
		}else{
			this.flightService.getFlightsJson(this.params).then( flights => this.successRespJson(flights),error => this.errorMessage = <any>error);   
		}
		//this.flightService.getFlights(this.params)
		  //  .subscribe( flights => this.successResp(flights),
				//                error => this.errorMessage = <any>error);   
		 

	}
	 
	 
	selectTab(tab_id: any) {
		this.activeTab = tab_id;
		if(tab_id == 1){
			this.returnDate = false;
		}else{
			this.returnDate = true;
		}
	 
	}
	 
	successRespJson(data:any)
	{
		let newArr = [];
		
		if(this.sliderMin!=0 || this.sliderMax!=20000){
			//filter for price
			
			if(typeof data != 'undefined' && data !=''){
				for(let i=0; i<=data.length-1;i++){
								
					let price_new =  Number(data[i].saleTotal.replace(/[^0-9\.-]+/g,""));
					
					if(price_new > this.sliderMin && price_new < this.sliderMax){
						newArr.push(data[i]);
					}
				}
				this.flightss = newArr;
			}else{
				this.rowMesz = "No record found";
			}
			
			
		}else{
			this.flightss = data;
		}
		this.flights= [];
		
		
	}
	 
	successResp(data:any)
	{
		
		let newArr = [];
		let alldata = data.trips.tripOption;
		if(this.sliderMin!=0 || this.sliderMax!=4000){
			//filter for price
			if(typeof alldata != 'undefined' && alldata !=''){
				for(let i=0; i<=alldata.length-2;i++){			
					let price_new =  Number(alldata[i].saleTotal.replace(/[^0-9\.-]+/g,""));
					
					if(price_new > this.sliderMin && price_new < this.sliderMax){
						newArr.push(alldata[i]);
					}
				}
			}else{
				this.rowMesz = "No record found";
			}
			
			this.flights = newArr;		
		}else{
			this.flights = data.trips.tripOption;
		}
		this.flightss= [];
	}
		
}
